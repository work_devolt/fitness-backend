<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Specialtactics\L5Api\Models\RestfulModel;

/**
 * @mixin \Eloquent
 * @mixin Builder
 */
class BaseModel extends RestfulModel
{
    /**
     * Acts like $withCount (eager loads relations), however only for immediate controller requests for that object
     * This is useful if you want to use "with" for immediate resource routes, however don't want these relations
     *  always loaded in various service functions, for performance reasons
     *
     * @deprecated Use  getItemWithCount() and getCollectionWith()
     * @var array Relations to load implicitly by Restful controllers
     */
    public static $localWithCount = null;

    public static $defaultSorts = '-created_at';

    public static $allowedSorts = [];

    public static $allowedAppends = [];

    public static $allowedIncludes = [];

    public static $allowedFilters = [];

    public static $allowedFields = [];

    /**
     * What count of relations should one model of this entity be returned with, from a relevant controller
     *
     * @var null|array
     */
    public static $itemWithCount = [];

    /**
     * What count of relations should a collection of models of this entity be returned with, from a relevant controller
     * If left null, then $itemWithCount will be used
     *
     * @var null|array
     */
    public static $collectionWithCount = null;

    protected $perPage = 15;

    /**
     * If using deprecated $localWith then use that
     * Otherwise, use $itemWith
     *
     * @return array
     */
    public static function getItemWithCount()
    {
        return static::$itemWithCount;
    }

    /**
     * If using deprecated $localWith then use that
     * Otherwise, if collectionWith hasn't been set, use $itemWith by default
     * Otherwise, use collectionWith
     *
     * @return array
     */
    public static function getCollectionWithCount()
    {
        if (!is_null(static::$collectionWithCount)) {
            return static::$collectionWithCount;
        } else {
            return static::$itemWithCount;
        }
    }

    public static function getDefaultSorts()
    {
        return static::$defaultSorts;
    }

    public static function getAllowedSorts()
    {
        return static::$allowedSorts;
    }

    public static function getAllowedAppends()
    {
        return static::$allowedAppends;
    }

    public static function getAllowedIncludes()
    {
        return static::$allowedIncludes;
    }

    public static function getAllowedFilters()
    {
        return static::$allowedFilters;
    }

    public static function getAllowedFields()
    {
        return static::$allowedFields;
    }
}
