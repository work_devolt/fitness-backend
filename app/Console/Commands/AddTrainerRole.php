<?php


namespace App\Console\Commands;


use App\Models\Role;
use Illuminate\Console\Command;

class AddTrainerRole extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'role:trainer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        Role::create(['name' => 'trainer', 'description' => 'trainer']);
    }
}
