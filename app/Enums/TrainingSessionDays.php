<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ODD()
 * @method static static EVEN()
 */
final class TrainingSessionDays extends Enum
{
    const EVEN =   'odd';
    const ODD =   'even';
}
