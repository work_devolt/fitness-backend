<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Requests;
use Illuminate\Support\Facades\DB;


class RequestsController extends Controller
{

    public static $model = Requests::class;

    public static $parentModel = null;

    public static $transformer = null;

    public function store(Request $request)
    {
        $kek = new Requests();
        $kek->name = $request['name'];
        $kek->phone = $request['phone'];
        $kek->save();
    }

    public function storePaymentDate(Request $request) {
        $date = $request->date;
        DB::table('payment_date')->insert(['date' => $date]);
    }

    public function getPaymentDate() {
        $date = DB::table('payment_date')->orderByDesc('id')->first();
        return $date ? $date->date : '';
    }
}
