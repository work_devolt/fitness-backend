<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubscriptionSellRequest;
use App\Models\Subscription;
use Illuminate\Http\Request;
use App\Models\TrainingSession;

class TrainingSessionController extends Controller
{

    public static $model = TrainingSession::class;

    public static $parentModel = null;

    public static $transformer = null;

    public function sell($uuid, SubscriptionSellRequest $request)
    {
        /** @var Subscription $model */
        $model = static::$model::findOrFail($uuid);

        $this->authorizeUserAction('update', $model);

        $validated = $request->validated();

        $model->sell($validated['payment_method']);

        if ($this->shouldTransform()) {
            $response = $this->response->item($model, $this->getTransformer());
        } else {
            $response = $model;
        }

        return $response;
    }


    public function getTrainings(Request $request) {

        $periodicity = $request->query('periodicity');
        $trainer_id = $request->query('trainer_id');


        $times = collect([
            [
                "time" => "9:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "9:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "10:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "10:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "11:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "11:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "12:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "12:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "13:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "13:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "14:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "14:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "15:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "15:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "16:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "16:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "17:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "17:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "18:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "18:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "19:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "19:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "20:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "20:30",
                "trainings" => collect([]),
            ],
            [
                "time" => "21:00",
                "trainings" => collect([]),
            ],
            [
                "time" => "21:30",
                "trainings" => collect([]),
            ],

        ]);


        $trainings = TrainingSession::all()->where('trainer_id', $trainer_id)->where('periodicity', $periodicity);


        foreach ($trainings as $item) {
//            $timeStamp = date( "H:i", strtotime($item->date_start));
            $timeStamp = $item->training_time;
            ($times->where('time', $timeStamp)->first()) ? ($times->where('time', $timeStamp)->first())['trainings']->push($item) : null;
        }


        return $times->toJson(JSON_PRETTY_PRINT);;
    }
}
