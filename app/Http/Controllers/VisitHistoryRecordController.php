<?php

namespace App\Http\Controllers;

use App\Models\BaseModel;
use App\Transformers\BaseTransformer;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use App\Models\VisitHistoryRecord;
use Spatie\QueryBuilder\QueryBuilder;

class VisitHistoryRecordController extends Controller
{
    /**
     * @var BaseModel|string The primary model associated with this controller
     */
    public static $model = VisitHistoryRecord::class;

    /**
     * @var BaseModel|string The parent model of the model, in the case of a child rest controller
     */
    public static $parentModel = null;

    /**
     * @var null|BaseTransformer The transformer this controller should use, if overriding the model & default
     */
    public static $transformer = null;

    public function getUniqueClients(Request $request) {
       $hallId = $request->query('hall');
       $startDate = $request->query('start');
       $endDate = $request->query('end');

       if ($hallId) {
           $query = VisitHistoryRecord::query()
               ->whereHas('client', function (Builder $query) use ($hallId) {
                   return $query->where('primary_hall_id', $hallId);
               })
               ->whereDate('created_at', '>=', $startDate)
               ->whereDate('created_at', '<=', $endDate)
               ->get();
       }                                                                                //todo
       else {
           $query = VisitHistoryRecord::query()
               ->whereDate('created_at', '>=', $startDate)
               ->whereDate('created_at', '<=', $endDate)
               ->get();
       }

       return $query ? $query->unique('client_id')->count() : 0;

    }
}
