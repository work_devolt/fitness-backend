<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);
        $this->call(HallsSeeder::class);
        $this->call(EmployeesSeeder::class);
        $this->call(UserStorySeeder::class);

        $this->call(ClientsSeeder::class);
        $this->call(LockersSeeder::class);
        $this->call(IssuesSeeder::class);
        $this->call(GroupsSeeder::class);
        $this->call(GroupScheduleSeeder::class);
        $this->call(ClientGroupSeeder::class);
        $this->call(TrainersSeeder::class);
        $this->call(SubscriptionsSeeder::class);
        $this->call(BarItemsSeeder::class);
        $this->call(VisitHistoryRecordsSeeder::class);
        $this->call(IssueDiscussionsSeeder::class);
        $this->call(IdentifiersSeeder::class);

    }
}
