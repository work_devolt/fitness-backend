<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\Enums\TrainingSessionDays;

class AddsWeekDaysToSession extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('training_sessions', function (Blueprint $table) {
            $table->string('periodicity')->default(TrainingSessionDays::EVEN)->nullable();
            $table->string('training_time')->nullable();
            $table->string('date_start')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('training_sessions', function (Blueprint $table) {
            $table->dropColumn(['periodicity']);
            $table->dropColumn(['training_time']);
        });
    }
}
