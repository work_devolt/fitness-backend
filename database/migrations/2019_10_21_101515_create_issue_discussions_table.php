<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIssueDiscussionsTable extends Migration
{

    const TABLE_NAME = 'issue_discussions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(static::TABLE_NAME, function (Blueprint $table) {
            $table->uuid('issue_discussion_id');

            $table->primary('issue_discussion_id');

            $table->uuid('user_id')->unsigned();
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');

            $table->uuid('issue_id')->unsigned();
            $table->foreign('issue_id')->references('issue_id')->on('issues')->onDelete('cascade');

            $table->text('text');

            $table->timestamps();
            // $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(static::TABLE_NAME);
    }
}
